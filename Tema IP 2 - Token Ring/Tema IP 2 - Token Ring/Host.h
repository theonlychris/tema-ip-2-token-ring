#pragma once
#include <iostream>
#include <string>
class Host
{
public:
	Host();
	Host(const std::string& ipAdress, const std::string& buffer);
	~Host();
public:
	friend std::ostream& operator<< (std::ostream& os, const Host& host);
public:
	std::string ipAdress;
	std::string buffer;
};


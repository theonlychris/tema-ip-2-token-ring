#include "Host.h"




Host::Host()
{
	// Empty
}

Host::Host(const std::string& ipAdress, const std::string& buffer) : ipAdress(ipAdress), buffer(buffer)
{
	// Empty
}


Host::~Host()
{
	// Empty
}

std::ostream& operator<<(std::ostream& os, const Host& host)
{
	os << host.ipAdress << ", " << host.buffer;
	return os;
}

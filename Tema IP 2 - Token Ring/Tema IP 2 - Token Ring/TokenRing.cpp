#include "TokenRing.h"



TokenRing::TokenRing()
{
	// Empty
}

TokenRing::TokenRing(const std::vector<std::string>& sources)
{
	this->host.ipAdress = sources[0];
	TokenRing* ring = new TokenRing();
	this->next = ring;
	TokenRing* startRing = this;

	for (int i = 1; i < sources.size() - 1; ++i)
	{
		/* Next destination */
		ring->next = new TokenRing();
		ring->host.ipAdress = sources[i];
		/* Switched to the next */
		ring = ring->next;
	}

	/* Make it circular */
	ring->next = startRing;
	ring->host.ipAdress = sources.at(sources.size()-1);
}


TokenRing::~TokenRing()
{
	// Empty
}

std::ostream& operator<<(std::ostream& os, const TokenRing& tokenRing)
{
	os << "[ ";
	const TokenRing* source = &tokenRing;
	TokenRing temp = tokenRing;
	TokenRing* temp_ptr = &temp;

	while (temp_ptr->next != source)
	{
		os << temp_ptr->host;
		temp_ptr = temp_ptr->next;
	}
	os << temp_ptr->host;
	os << tokenRing.host << "]";
	return os;
}

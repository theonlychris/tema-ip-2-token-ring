#include "Utils.hpp"

namespace Utils
{
	/* Gets a random Ip Adress */
	std::string Utils::getRandomIpAdress()
	{
		std::string first = std::to_string(Random::get(2, 217));
		std::string second = std::to_string(Random::get(0, 255));
		std::string third = std::to_string(Random::get(0, 255));
		std::string fourth = std::to_string(Random::get(0, 255));

		return first + "." + second + "." + third + "." + fourth;
	}


}
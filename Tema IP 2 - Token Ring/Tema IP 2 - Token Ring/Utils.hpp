#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include "Dependencies/random.hpp"
using Random = effolkronium::random_static;

/* Overloads the << Operator to show the contents of a vector */
template <typename T>
std::ostream& operator << (std::ostream& os, const std::vector<T>& myVector)
{
	os << "[";
	for (int i = 0; i < myVector.size(); ++i) {
		os << myVector[i];
		if (i != myVector.size() - 1)
			os << ", ";
	}
	os << "]" << std::endl;
	return os;
}

namespace Utils
{
	/* Gets a random Ip Adress */
	inline std::string getRandomIpAdress()
	{
		std::string first = std::to_string(Random::get(2, 217));
		std::string second = std::to_string(Random::get(0, 255));
		std::string third = std::to_string(Random::get(0, 255));
		std::string fourth = std::to_string(Random::get(0, 255));

		return first + "." + second + "." + third + "." + fourth;
	}

	/* Gets a random string */
	inline std::string getRandomString(int stringSize)
	{
		std::string characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		std::string randomString = "";
		int charSize = characters.size() - 1;
		for (int i = 0; i < stringSize; i++)
		{
			randomString += characters[Random::get(1,charSize)];
		}
		return randomString;
	}
}


#endif // !UTILS_HPP





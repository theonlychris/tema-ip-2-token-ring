#include "Token.h"




Token::Token()
{
	// Empty
}

Token::Token(const std::string& sourceIp, const std::string& destinationIp, const std::string& message = Utils::getRandomString(10)) : sourceIp(sourceIp), destinationIp(destinationIp), message(message)
{
	// Empty
}


Token::~Token()
{
	// Empty
}

std::ostream& operator<<(std::ostream& os, const Token& token)
{
	os << "[";
	os << token.sourceIp << ", " << token.destinationIp << ", " << token.message;
	os << "]" << std::endl;
	
	return os;
}

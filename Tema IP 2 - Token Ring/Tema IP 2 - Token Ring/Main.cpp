#include <iostream>
#include <string>
#include "Utils.hpp"
#include "TokenRing.h"
#include "Token.h"

int main()
{
	std::vector<std::string> ipSources;
	std::vector<std::string> ipDestinations;
	std::vector<Token> tokens;

	/* Getting 10 Random Ip Adresses */
	for (int i = 0; i < 10; ++i)
		ipSources.push_back(Utils::getRandomIpAdress());

	/* Copying the current Ips */
	ipDestinations = ipSources;

	for (int i = 0; i < 10; ++i)
		tokens.push_back(Token(ipSources[i], ipDestinations[Random::get(0, 9)], Utils::getRandomString(10)));

	/* Shuffling the ipDestinations vector */
	Random::shuffle(ipDestinations);

	std::cout << "The Source Ips: " << std::endl;
	std::cout << ipSources << std::endl;
	std::cout << "The Destination Ips: " << std::endl;
	std::cout << ipDestinations << std::endl;
	std::cout << "The Tokens: " << std::endl;
	std::cout << tokens << std::endl;

	/* Initializing the token ring */
	TokenRing * tokenRing = new TokenRing(ipSources);

	std::cout << "The Token Ring Network: " << std::endl;
	std::cout << *tokenRing << std::endl << std::endl;

	for (auto& token : tokens)
	{
		//Token token = tokens[0];

		std::cout << "The Token: ";
		std::cout << token << std::endl;

		if (token.isFree == false)
			return 3;

		/* Searching the host in the token ring to start sending the token*/
		while (token.sourceIp != tokenRing->host.ipAdress)
			tokenRing = tokenRing->next;

		/* Taking the source of the tokenRing*/
		TokenRing * source = tokenRing;

		/* The Path that the token took */
		std::string path = "";
		while (tokenRing->host.ipAdress != token.destinationIp)
		{
			path += tokenRing->host.ipAdress + " -> ";
			tokenRing = tokenRing->next;
			path += tokenRing->host.ipAdress + "\n";
		}
		/* The host got the message succesfully */
		tokenRing->host.buffer = token.message;
		path += "The Message Received = " + tokenRing->host.buffer + "\n";
		/* Token is now delivered and !Free*/
		token.isDelivered = true;
		token.isFree = false;
		/* Going back to the source */
		while (tokenRing != source)
			tokenRing = tokenRing->next;
		/* Showing the path + the message received*/
		std::cout << "The Path: " << std::endl;
		std::cout << path << std::endl;
	}
	



	std::cin.get();
	return 0;

}

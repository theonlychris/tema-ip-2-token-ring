#pragma once
#include <memory>
#include <vector>
#include "Host.h"
class TokenRing
{
public:
	TokenRing();
	TokenRing(const std::vector<std::string>& sources);
	~TokenRing();
public: 
	friend std::ostream& operator<<(std::ostream& os, const TokenRing& tokenRing);
public:
	TokenRing* next;
	Host host;
};


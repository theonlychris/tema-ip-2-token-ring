#pragma once
#include "Utils.hpp"


class Token
{
public:
	Token();
	Token(const std::string& sourceIp, const std::string& destinationIp, const std::string& message);
	~Token();
public:
	friend std::ostream& operator<< (std::ostream& os, const Token& token);
public:
	std::string sourceIp;
	std::string destinationIp;
	std::string message;
	bool isFree = true;
	bool isDelivered = false;
};

